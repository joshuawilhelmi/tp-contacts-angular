import { Component, OnInit } from '@angular/core';
import { Contact } from '../model/contact';
import { ContactService } from '../contact.service';
import { Civilite } from '../model/civilite.enum';
import { TypeTelephone } from '../model/type-telephone.enum';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {
  displayedColumns: string[] = ['civilite', 'nom', 'voir'];
  contacts: Contact[]

  constructor(private contactService: ContactService) { }

  ngOnInit() {
    this.contactService.getContacts().subscribe(
      (contacts: Contact[]) => {
        this.contacts = contacts.map(Contact.fromWsResponse)
        console.log(this.contacts)
      }
    )
  }

}
