import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ContactService } from '../contact.service';
import { Contact } from '../model/contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  
  contact: Contact

  constructor(private route: ActivatedRoute, private contactService: ContactService) {

  }

  ngOnInit() {
    this.route.params.subscribe(
      p => {
        console.log(p.id)
        // Récupérer mes informations contact
        this.contactService.getContact(p.id).subscribe(
          (c: any) => this.contact = Contact.fromWsResponse(c)
        )
      }
    )
  }

}
