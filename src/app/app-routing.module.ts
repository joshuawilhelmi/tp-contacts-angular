import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { ContactsListComponent } from './contacts-list/contacts-list.component';

const routes: Routes = [
  { path: "", pathMatch: 'full', redirectTo: '/contacts' },
  { path: "contacts", component: ContactsListComponent },
  { path: "contacts/:id", component: ContactComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
