import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  getContacts() {
    return this.http.get(environment.backUrl + '/contacts')
  }

  getContact(id: number) {
    return this.http.get(environment.backUrl + '/contacts/' + id)
  }
}
