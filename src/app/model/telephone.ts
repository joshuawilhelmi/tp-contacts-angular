import { TypeTelephone } from "./type-telephone.enum";

export class Telephone {
    numero: string
    type: TypeTelephone
}