export enum TypeTelephone {
    Portable,
    Domicile,
    Professionnel
}