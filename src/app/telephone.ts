import { TypeTelephone } from "./model/type-telephone.enum";

export class Telephone {
    number: number
    type: TypeTelephone
}